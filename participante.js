// --------------------------------------------------------
// participante.js
// --------------------------------------------------------

//Requisitos
var zmq = require('zeromq')

//Variables globales
var id
var id_contacto = -1
var grupo = []
var colegas

//Obteniendo los parámetros de entrada
if (process.argv.length > 4 | process.argv.length < 3)
{
    console.log("Parámetros de entrada incorrectos")
    console.log("Modo de ejecución: node participante.js id id_contacto")
    process.exit(1)
}
else if (process.argv.length == 3)
{
    id = process.argv[2]
}
else //process.argv.length == 4
{
    id = process.argv[2]
    id_contacto = process.argv[3]
}

class Colegas
{
    constructor(id_, actualizar_)
    {
        //Sockets        
        this.subscriber = zmq.socket('sub')  
        this.subscriber.subscribe("")      

        //Haciendo bind con su SUB
        this.subscriber.bind('tcp://127.0.0.1:' + id_, function(err) {
            if(err)
            {
                console.log("Error: " + err)
            }
            else
            {
                console.log("Haciendo bind desde el SUB desde el puerto " + id_ + " ...")
            }                
        })  

        this.publisher = zmq.socket('pub')

        this.listaYaConectados = [id_]

        this.subscriber.on("message", function(msgServer) {
            console.log("Hemos recibido un mensaje para que nos actualicemos")
            var msgServerJSON = JSON.parse(msgServer)
            if (msgServerJSON.func_name == "ACTUALIZAR")
            {
                console.log("Llamando a actualizar")
                actualizar_(msgServerJSON.members)
            }
        })

        //Capturando el mensaje SIGINT
        process.on('SIGINT', function() {
            console.log ("Evento SIGINT capturado")
            if (this.publisher)
            {
                this.publisher.close()
                console.log("PUB Socket cerrado")
            }
            if (this.subscriber)
            {
                this.subscriber.close()
                console.log("SUB Socket cerrado") 
            }

            process.exit(0)
        })
    }

    actualizar_proxy(g_)
    {
        this.conectarConNuevos(g_) 

        var msgColegasJSON = {
            func_name: "ACTUALIZAR",
            members: g_
        }
        var msgToSend = JSON.stringify(msgColegasJSON)

        console.log("Enviando mensaje para que se actualicen el resto de colegas")
        var end = Date.now() + 5000
        while (Date.now() < end) ;
        this.publisher.send(msgToSend)
    }

    conectarConNuevos(g_)
    {
        for(var i=0; i<g_.length; ++i) {
            if (this.listaYaConectados.includes(g_[i]) == false)
            {
                console.log("Haciendo connect desde el PUB con el puerto " + g_[i])
                this.publisher.connect("tcp://127.0.0.1:" + g_[i])   
                this.listaYaConectados.push(g_[i])
            }            
        }
    }
    
}

//Funciones semanticas
var actualizarfuncion = function actualizar_real(g_)
{
    console.log("Actualizando (actualizar real)")
    if (sonIguales(g_, grupo) == false)
    {       
        for (var i = 0;i < g_.length;i++)
        {
            if (grupo.includes(g_[i]) == false)
            {
                grupo.push(g_[i])
            }
        }
        colegas.actualizar_proxy(grupo)
        console.log("Grupo actualizado a")
    }
    else
    {
        console.log("El grupo ya estaba actualizado a")
    }

    pintar_grupo(grupo)

}

function main(id_, id_contacto_)
{
    grupo.push(id_)
    colegas = new Colegas(id_, actualizarfuncion)
    if (id_contacto_ != -1)
    {
        actualizarfuncion([id_contacto_])
    }
}

main(id, id_contacto)

//Funciones auxiliares
function sonIguales(arr1, arr2) {
    if(arr1.length !== arr2.length)
    {
        return false;
    }

    for (var i = 0;i < arr1.length; i++)
    {
        if (arr2.includes(arr1[i]) == false)
        {
            return false
        }
    }

    for (var i = 0;i < arr2.length; i++)
    {
        if (arr1.includes(arr2[i]) == false)
        {
            return false
        }
    }

    return true;
}

function pintar_grupo(grupo)
{
    str = "{"
    for(var i = grupo.length - 1; i--;) {
        str = str + grupo[i]
        str = str + ","
    }

    str  = str + grupo[grupo.length - 1] + "}"

    console.log(str)
}
