# Tarea 3
#### Autor: Jorge Cabrejas Peñuelas

## Objetivo
El objetivo de este proyecto es el diseño y la implementación de un algoritmo p2p para formar un grupo.

## Requisitos
Para poder llevar a cabo el objetivo que se plantea es necesario tener instalado nodejs y la librería zmp.

## Descripción de la solución
La solución planteada se basa en la utilización de una clase que hace de proxy aislando la comunicación del resto del 
programa. Como parte de la implementación, para poder usar los sockets PUB/SUB se plantea un esquema inverso donde los 
sockets PUB hacen *connect* y los sockets SUB hacen *bind*. 

### Funciones de la solución
En el siguiente esquema se muestran las diferentes funciones generales con sus interacciones:

<img src="figuras/diagrama_funciones.png" width="800">

### Ejemplo de una interacción
En la siguiente figura se muestra un ejemplo de interacciones entre las diferentes funciones mostradas en la figura 
anterior. En la primera fase, un participante A quiere crear un grupo, pero no tiene ningún contacto, así que él es el 
único miembro del grupo. En la segunda fase, un participante B contacta con el participante A y ambos forman parte del 
grupo. En la tercera fase, un participante C contacta con el participante B y éste informa a todos los participantes que
 se ha unido un nuevo participante al grupo, conociendo desde ese momento cuales son los miembros del grupo. 

<img src="figuras/diagrama_interacciones.png" width="1200">

### Implementación de la solución
La siguiente figura muestra la implementación de la función main():

<img src="figuras/main.png" width="800">

La siguiente figura muestra la implementación del contructor de la clase Colegas que se usa para aislar la comunicación
del resto del programa:

<img src="figuras/constructor.png" width="800">

En la siguiente figura se muestran los métodos de actualizar real y proxy, así como la función de la clase colegas que
sirve para conectarse al resto de participantes no conectados todavía.

<img src="figuras/actualizar.png" width="800">

En la siguiente figura se muestra la función de *callback* que se llama cuando se reciben un mensaje por el socket SUB:

<img src="figuras/recepcion_sub.png" width="800">

## Ejemplo de ejecución

Finalmente, se muestra un pantallazo de un ejemplo de ejecución donde se tienen tres participantes. En un primer 
momento, un participante publica en el puerto 5000, pero no se conecta con nadie. Después, otro participante publica en
el puerto 4000 y se subscribe en el puerto 5000 del anterior participante. Finalmente, el último participante publica en
el puerto 3000 y se subscribe al puerto 4000 del segundo participante, actualizándose a continuación todos el grupo.

<img src="figuras/resultado.png" width="800">

